//
//  Fun.m
//  FunPrj
//
//  Created by Thuy Dao on 2/9/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import "FunPost.h"

@implementation FunPost

/**
 * Method initWithDictionary
 */

- (id)initWithDictionary:(NSDictionary*)dict
{
    if (self = [super init]) {
        
        self.idi            = [NSString string:[dict objectForKey:@"id"]];
        self.ytid           = [NSString string:[dict objectForKey:@"ytid"]];
        self.wm             = [NSString string:[dict objectForKey:@"wm"]];
        self.url            = [NSString string:[dict objectForKey:@"url"]];
        self.type           = [NSString string:[dict objectForKey:@"type"]];
        self.ts             = [NSString string:[dict objectForKey:@"ts"]];
        self.status         = [NSString string:[dict objectForKey:@"status"]];
        self.slug           = [NSString string:[dict objectForKey:@"slug"]];
        self.iid            = [NSString string:[dict objectForKey:@"iid"]];
        self.is_iphone      = ([[dict objectForKey:@"is_iphone"] integerValue] == 0) ? NO: YES;
        self.name           = [NSString string:[dict objectForKey:@"name"]];
        self.ats            = [NSString string:[dict objectForKey:@"ats"]];
        self.content        = [NSString string:[dict objectForKey:@"content"]];
        self.content_uf     = [NSString string:[dict objectForKey:@"content_uf"]];
        
        self.user           = [[FunUser alloc] initWithDictionary:[dict objectForKey:@"u"]];
        
        NSDictionary* counter = [dict objectForKey:@"counter"];
        self.strCommentNum = [NSString string:[counter objectForKey:@"c"]];
        self.strLikeNum = [NSString string:[counter objectForKey:@"vote"]];
        
    }
    return self;
}

/**
 * Method desc
 */

- (NSString*)desc
{
    return [NSString stringWithFormat:@"idi:\t%@ \nytid:\t%@ \nwm:\t%@ \nurl:\t%@ \ntype:\t%@ \nts:\t%@ \nstatus:\t%@ \nslug:\t%@ \niid:\t%@ \nis_phone:\t%d \nname:\t%@ \nats:\t%@ \ncontent:\t%@ \ncontent_url:\t%@ \n ", self.idi,self.ytid,self.wm,self.url,self.type,self.ts,self.status,self.slug,self.iid,self.is_iphone,self.name,self.ats,self.content,self.content_uf];
}


/**
 * Method isImage
 */

- (BOOL)isImage
{
    if ([self.type integerValue] == image) return YES;
    return NO;
}

/**
 * Method isVideo
 */

- (BOOL)isVideo
{
    if ([self.type integerValue] == video) return YES;
    return NO;
}

/**
 * Method isQuestion
 */

- (BOOL)isQuestion
{
    if ([self.type integerValue] == question) return YES;
    return NO;
}

/**
 * Method isStory
 */

- (BOOL)isStory
{
    if ([self.type integerValue] == story) return YES;
    return NO;
}

/**
 * Method urlImage
 */

- (NSString*)urlImage
{
    return [NSString stringWithFormat:@"%@%@",KHostImage,self.url];
}

/**
 * Method urlVideo
 */

- (NSString*)urlVideo
{
    return self.url;
}

/**
 * Method titleLabel
 */

- (NSString*)titleLabel
{
    if (![self.name isEqualToString:@""]) {
        return self.name;
    }
    else return self.content;
}


@end
