//
//  Fun.h
//  FunPrj
//
//  Created by Thuy Dao on 2/11/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FunUser : NSObject

/**
 * Method initWithDictionary
 */

- (id)initWithDictionary:(NSDictionary*)dictionary;

/**
 * property: idi
 */

@property (nonatomic, retain) NSString* idi;

/**
 * property: iid
 */

@property (nonatomic, retain) NSString* iid;

/**
 * property: lname
 */

@property (nonatomic, retain) NSString* lname;

/**
 * property: name
 */

@property (nonatomic, retain) NSString* name;

/**
 * property: avatar
 */

@property (nonatomic, retain) NSString* avatar;

/**
 * property: ts
 */

@property (nonatomic, retain) NSString* ts;

/**
 * property: p
 */

@property (nonatomic, retain) NSString* p;

/**
 * property: c
 */

@property (nonatomic, retain) NSString* c;

/**
 * property: k
 */

@property (nonatomic, retain) NSString* k;

/**
 * property: vd
 */

@property (nonatomic, retain) NSString* vd;


@end
