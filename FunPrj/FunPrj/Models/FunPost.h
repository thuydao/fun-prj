//
//  Fun.h
//  FunPrj
//
//  Created by Thuy Dao on 2/9/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FunUser.h"

//1. Truyện cười 2: Image 3: Video 4: Câu đố vui
enum {
    story = 1,
    image = 2,
    video = 3,
    question = 4
    };
typedef NSInteger typePost;

@interface FunPost : NSObject

/**
 * property: ats
 */

@property (nonatomic, retain) NSString* ats;

/**
 * property: content
 */

@property (nonatomic, retain) NSString* content;

/**
 * property: iid
 */

@property (nonatomic, retain) NSString* iid;

/**
 * property: is_iphone
 */

@property (nonatomic, assign) BOOL is_iphone;

/**
 * property: name
 */

@property (nonatomic, retain) NSString* name;

/**
 * property: slug
 */

@property (nonatomic, retain) NSString* slug;

/**
 * property: status
 */

@property (nonatomic, retain) NSString* status;

/**
 * property: ts
 */

@property (nonatomic, retain) NSString* ts;

/**
 * property: type
 */

@property (nonatomic, retain) NSString *type;

/**
 * property: url
 */

@property (nonatomic, retain) NSString* url;

/**
 * property: wm
 */

@property (nonatomic, retain) NSString* wm;

/**
 * property: id
 */

@property (nonatomic, retain) NSString* idi;


/**
 * property: ytid
 */

@property (nonatomic, retain) NSString* ytid;

/**
 * property: content_uf
 */

@property (nonatomic, retain) NSString* content_uf;

/**
 * property: user
 */

@property (nonatomic, retain) FunUser* user;


/**
 * property: strLikeNum
 */

@property (nonatomic, retain) NSString* strLikeNum;

/**
 * property: strCommentNum
 */

@property (nonatomic, retain) NSString* strCommentNum;


#pragma mark -
#pragma mark - funtion

/**
 * Method initWithDictionary
 */

- (id)initWithDictionary:(NSDictionary*)dict;

/**
 * Method desc
 */

- (NSString*)desc;

/**
 * Method isImage
 */

- (BOOL)isImage;

/**
 * Method isVideo
 */

- (BOOL)isVideo;

/**
 * Method isQuestion
 */

- (BOOL)isQuestion;

/**
 * Method isStory
 */

- (BOOL)isStory;

/**
 * Method urlImage
 */

- (NSString*)urlImage;

/**
 * Method urlVideo
 */

- (NSString*)urlVideo;

/**
 * Method titleLabel
 */

- (NSString*)titleLabel;

@end
