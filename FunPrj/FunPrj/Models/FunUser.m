//
//  Fun.m
//  FunPrj
//
//  Created by Thuy Dao on 2/11/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import "FunUser.h"

@implementation FunUser

- (id)initWithDictionary:(NSDictionary*)dictionary
{
    if (self = [super init]) {
        self.idi = [NSString string:[dictionary objectForKey:@"id"]];
        self.iid = [NSString string:[dictionary objectForKey:@"iid"]];
        self.lname = [NSString string:[dictionary objectForKey:@"lname"]];
        self.name = [NSString string:[dictionary objectForKey:@"name"]];
        self.avatar = [NSString string:[dictionary objectForKey:@"avatar"]];
        self.ts = [NSString string:[dictionary objectForKey:@"ts"]];
        
        NSDictionary* counter = [dictionary objectForKey:@"counter"];
        
        self.p = [NSString string:[counter objectForKey:@"p"]];
        self.c = [NSString string:[counter objectForKey:@"c"]];
        self.k = [NSString string:[counter objectForKey:@"k"]];
        self.vd = [NSString string:[counter objectForKey:@"vd"]];
    }
    return self;
}


@end
