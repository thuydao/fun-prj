//
//  FunAppDelegate.h
//  FunPrj
//
//  Created by Thuy Dao on 1/15/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import <UIKit/UIKit.h>

@class JASidePanelController;

@interface FunAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) JASidePanelController *viewController;

@end
