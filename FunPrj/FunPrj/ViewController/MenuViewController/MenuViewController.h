//
//  MenuViewController.h
//  FunPrj
//
//  Created by Thuy Dao on 1/15/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITableViewCell+NEW.h"

@interface MenuViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate>
{
    
}

/**
 * property: dataSource
 */

@property (nonatomic, retain) NSMutableArray* dataSource;

/**
 * property IBOutlet:myTable
 */

@property (nonatomic, retain) IBOutlet UITableView *myTable;

@end
