//
//  MenuViewController.m
//  FunPrj
//
//  Created by Thuy Dao on 1/15/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import "MenuViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.dataSource = [NSMutableArray new];
    [self fetching];
}

/**
 * Method fetching
 */

- (void)fetching
{
    NSMutableArray* sectio0 = [NSMutableArray new];
    NSMutableArray* sectio1 = [NSMutableArray new];
    NSMutableArray* sectio2 = [NSMutableArray new];
    
    NSDictionary* dic00 = [[NSDictionary alloc] initWithObjectsAndKeys:@"Mới",@"name", nil];
    NSDictionary* dic01 = [[NSDictionary alloc] initWithObjectsAndKeys:@"Hot",@"name", nil];
    NSDictionary* dic02 = [[NSDictionary alloc] initWithObjectsAndKeys:@"Cũ",@"name", nil];
    
    NSDictionary* dic10 = [[NSDictionary alloc] initWithObjectsAndKeys:@"Video",@"name", nil];
    
    NSDictionary* dic20 = [[NSDictionary alloc] initWithObjectsAndKeys:@"Tuỳ Chọn",@"name", nil];
    NSDictionary* dic21 = [[NSDictionary alloc] initWithObjectsAndKeys:@"Ứng dụng hay",@"name", nil];
    
    [sectio0 addObject:dic00];
    [sectio0 addObject:dic01];
    [sectio0 addObject:dic02];
    
    [sectio1 addObject:dic10];
    
    [sectio2 addObject:dic20];
    [sectio2 addObject:dic21];
    
    [self.dataSource addObject:sectio0];
    [self.dataSource addObject:sectio1];
    [self.dataSource addObject:sectio2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.dataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableArray *arr = [self.dataSource objectAtIndex:section];
    return [arr count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"cell0";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        [cell disableSelected];
    }
    NSMutableArray *arr = [self.dataSource objectAtIndex:indexPath.section];
    NSDictionary* dic = [arr objectAtIndex:indexPath.row];
    [cell labelWithTag:1].text = [dic objectForKey:@"name"];
    return cell;
}

@end
