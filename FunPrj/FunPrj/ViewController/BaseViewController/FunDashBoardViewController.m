//
//  FunDashBoardViewController.m
//  FunPrj
//
//  Created by Thuy Dao on 2/9/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import "FunDashBoardViewController.h"
#import "FunPost.h"

@interface FunDashBoardViewController ()

@end

@implementation FunDashBoardViewController
@synthesize dataSource;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NSLocalizedString(@"Hoibi.net",);
    [self initView];
}

/**
 * Method initView
 */

- (void)initView
{
    self.dataSource = [NSMutableArray new];
    self.dictCell = [NSMutableDictionary new];
    self.page = 1;
    self.prefix = [NSString stringWithFormat:@"new/%ld",(long)self.page];
    
    [self setupBanner];
}

/**
 * Method setupBanner
 */

- (void)setupBanner
{
    // Do any additional setup after loading the view, typically from a nib.
    
    // Create a view of the standard size at the bottom of the screen.
    // Available AdSize constants are explained in GADAdSize.h.
    bannerView_ = [[GADBannerView alloc]
                   initWithFrame:CGRectMake(0.0,
                                            self.view.frame.size.height -
                                            GAD_SIZE_320x50.height,
                                            GAD_SIZE_320x50.width,
                                            GAD_SIZE_320x50.height)];
    
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    //Alan's iphone site
    bannerView_.adUnitID = KBanerID;
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    bannerView_.rootViewController = self;
    bannerView_.delegate = (id)self;
    [self.view addSubview:bannerView_];
    
    // Initiate a generic request to load it with an ad
    
    GADRequest *request = [GADRequest request];
    request.testing = YES;
    [bannerView_ loadRequest: request];
}

// We've received an ad successfully.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    NSLog(@"Received ad successfully");
}

- (void)adView:(GADBannerView *)view
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Failed to receive ad with error: %@", [error localizedFailureReason]);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!animated) [self callAPI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/**
 * Method callAPI
 */

- (void)callAPI
{
    //  init info
    NSDictionary *info = [[NSDictionary alloc] initWithObjectsAndKeys:@"1",@"_cl_submit",
                          @"1",@"_cl_modal_ajax",
                          @"1",@"_cl_rest",
                          @"43424",@"_appId",
                          @"1000",@"_story_page",
                          @"1",@"_cl_mobi", nil];
    
    [FunService service:info prefix:self.prefix completed:^(id object, NSError *err){
        if (err) {
            NSLog(@"err :/n %@",[err description]);
        }
        
        else {
            if ([[object objectForKey:@"success"] integerValue] == 1) {
                NSLog(@" --------SUCESS------- ");
                NSString *total = [NSString string:[object objectForKey:@"total"]];
                NSString *count = [NSString string:[object objectForKey:@"count"]];
#if KTest
                NSLog(@"total :\n %@", total);
                NSLog(@"count :\n %@", count);
#endif
                NSArray *res = [object objectForKey:@"result"];
                [self processData:res];
            }
            else NSLog(@" --------FAIL------- ");
        }
    }];
}

#pragma mark -
#pragma mark - untils

- (void)processData:(NSArray*)arr
{
    NSMutableArray* convert = [self getArrDataSource:arr];
    
    if (self.page == 0 || self.page == 1) {
        // refresh
        self.dataSource = [NSMutableArray new];
        self.dataSource = [convert copy];
        [self refreshData];
    }
    
    else {
        // see more
        [self.dataSource addObjectsFromArray:convert];
        [self seeMore];
    }
}


/**
 * Method getArrDataSource
 */

- (NSMutableArray*)getArrDataSource:(NSArray*)arr
{
    NSMutableArray *data = [NSMutableArray new];
    for (NSDictionary* dic in arr) {
        FunPost *FPostObj = [[FunPost alloc] initWithDictionary:dic];
#if KTest
        if ([FPostObj isImage]) {
            NSLog(@"fp :\n %@", [FPostObj desc]);
        }
#endif
        
        [data addObject:FPostObj];
    }
    return data;
}

#pragma mark -
#pragma mark - ProcessData

/**
 * Method refreshData
 */

- (void)refreshData
{
    
}

/**
 * Method seeMore
 */

- (void)seeMore
{
    
}

@end
