//
//  FunDashBoardViewController.h
//  FunPrj
//
//  Created by Thuy Dao on 2/9/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADBannerView.h"
#import "UITableViewCell+NEW.h"

@interface FunDashBoardViewController : BaseViewController

{
    GADBannerView *bannerView_;
}

/**
 * property: dictCell
 */

@property (nonatomic, retain) NSMutableDictionary* dictCell;

/**
 * property: prefix
 */

@property (nonatomic, retain) NSString* prefix;

/**
 * property: page
 */

@property (nonatomic, assign) NSInteger page;

/**
 * property: dataSource
 */

@property (nonatomic, retain) NSMutableArray* dataSource;

/**
 * Method callAPI
 */

- (void)callAPI;

#pragma mark -
#pragma mark - ProcessData

/**
 * Method refreshData
 */

- (void)refreshData;

/**
 * Method seeMore
 */

- (void)seeMore;

#pragma mark -
#pragma mark - untils

/**
 * Method getImage
 */

- (void)getImage;

/**
 * Method getArrDataSource
 */

- (NSMutableArray*)getArrDataSource:(NSArray*)arr;

@end
