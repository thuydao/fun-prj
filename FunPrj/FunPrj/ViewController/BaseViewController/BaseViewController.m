//
//  BaseViewController.m
//  FunPrj
//
//  Created by Thuy Dao on 1/15/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - CONFIGURE

/*
 * Method setupPull
 */

- (void)setupPull:(UITableView*)table
{
    // set pull
    //-- check version ios
    float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    __weak BaseViewController *myself = self;
    
    if (sysVer < 6.0) {
        // setup pull-to-refresh
        [table addPullToRefreshWithActionHandler:^{
            [myself handleRefresh:nil];
        }];
    }
    else {
        //-- add refresh control to tableview
        refreshControl = [[ODRefreshControl alloc] initInScrollView:table];
        [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
        refreshControl.tintColor = [UIColor colorWithRed:162/256.0f green:20/256.0f blue:18/256.0f alpha:1];
        
        [table addSubview:refreshControl];
    }
    
    // setup infinite scrolling
    [table addInfiniteScrollingWithActionHandler:^{
        [myself insertRowAtBottom];
    }];
    // end set pull
}

/*
 * Method handleRefresh
 */

- (void)handleRefresh:(ODRefreshControl*)refreshControl
{
    
}

/*
 * Method insertRowAtBottom
 */

- (void)insertRowAtBottom
{
    
}


@end
