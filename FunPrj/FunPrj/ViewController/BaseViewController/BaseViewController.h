//
//  BaseViewController.h
//  FunPrj
//
//  Created by Thuy Dao on 1/15/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ODRefreshControl.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import "UIScrollView+SVPullToRefresh.h"

@interface BaseViewController : UIViewController

{
    ODRefreshControl *refreshControl;
}

/*
 * Method setupPull
 */

- (void)setupPull:(UITableView*)table;

@end
