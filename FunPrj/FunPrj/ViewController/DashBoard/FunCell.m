//
//  FunCell.m
//  FunPrj
//
//  Created by Thuy Dao on 2/9/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import "FunCell.h"
#import "UIImageView+WebCache.h"

#define KheightHeader 44
#define KheightVideo 200
#define kheightButtom 44


@interface UIImage (FunResize)

- (UIImage *) resizedImageByWidth:  (NSUInteger) width;

@end

@implementation UIImage (FunResize)

- (CGImageRef) CGImageWithCorrectOrientation
{
    if (self.imageOrientation == UIImageOrientationDown) {
        //retaining because caller expects to own the reference
        CGImageRetain([self CGImage]);
        return [self CGImage];
    }
    UIGraphicsBeginImageContext(self.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (self.imageOrientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, 90 * M_PI/180);
    } else if (self.imageOrientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, -90 * M_PI/180);
    } else if (self.imageOrientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, 180 * M_PI/180);
    }
    
    [self drawAtPoint:CGPointMake(0, 0)];
    
    CGImageRef cgImage = CGBitmapContextCreateImage(context);
    UIGraphicsEndImageContext();
    
    return cgImage;
}

- (UIImage *) drawImageInBounds: (CGRect) bounds
{
    UIGraphicsBeginImageContext(bounds.size);
    [self drawInRect: bounds];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizedImage;
}


- (UIImage *) resizedImageByWidth:  (NSUInteger) width
{
    CGImageRef imgRef = [self CGImageWithCorrectOrientation];
    CGFloat original_width  = CGImageGetWidth(imgRef);
    CGFloat original_height = CGImageGetHeight(imgRef);
    CGFloat ratio = width/original_width;
    CGImageRelease(imgRef);
    return [self drawImageInBounds: CGRectMake(0, 0, width, round(original_height * ratio))];
}

@end





























@implementation FunCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

#pragma mark -
#pragma mark - viewFormNIb

- (UIView*)getHeaderFormNib
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"CustomView"
                                                   owner:nil options:nil];
    return (id)[views objectAtIndex:0];
}

- (UIView*)getButtomViewFromNib
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"CustomView"
                                                   owner:nil options:nil];
    return (id)[views objectAtIndex:1];
}

/**
 * Method setup
 */

- (void)setup
{
    self.index = 44;
    self.heightCell = 0;
#if 0
    if ([self.post isImage]) {
        if (self.ivImage == nil) [self setupImgae];
    }
    if ([self.post isVideo]) {
       if (self.wvVideo == nil) [self embedYouTube:[self.post urlVideo] frame:CGRectMake(0, KheightHeader, 320, KheightVideo)];
    }
    
    if (self.headerView == nil) [self fetchDataHeaderView];
    if (self.buttomview == nil) [self fetchDataButtomView];
#else
    if ([self.post isImage]) {
        if (self.ivImage == nil) [self setupImgae];
    }
    if ([self.post isVideo]) {
        if (self.wvVideo == nil) [self embedYouTube:[self.post urlVideo] frame:CGRectMake(0, KheightHeader, 320, KheightVideo)];
    }
    [self fetchDataHeaderView];
//    [self fetchDataButtomView];

#endif
}

- (void)fetchDataButtomView
{
    self.buttomview = [self getButtomViewFromNib];
    
    //settaget
    [[self.buttomview buttonWithTag:101] addTarget:self action:@selector(like:) forControlEvents:UIControlEventTouchUpInside];
     [[self.buttomview buttonWithTag:102] addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
     [[self.buttomview buttonWithTag:103] addTarget:self action:@selector(save:) forControlEvents:UIControlEventTouchUpInside];
     [[self.buttomview buttonWithTag:104] addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
    
    // settext
    [self.buttomview labelWithTag:201].text = self.post.strLikeNum;
    [self.buttomview labelWithTag:202].text = self.post.strCommentNum;
    
    
    [self addSubview:self.buttomview];
}

- (void)fetchDataHeaderView
{
    self.headerView = [self getHeaderFormNib];
    self.headerView.frame = CGRectMake(0, 0, 320, KheightHeader);
    [self addSubview:self.headerView];
    [self.headerView labelWithTag:2].text = [self.post titleLabel];
    [[self.headerView imageViewWithTag:1] setImageWithURL:[NSURL URLWithString:self.post.user.avatar] placeholderImage:nil completed:^(UIImage *image, NSError *error,SDImageCacheType cacheType) {
        
    }];
}



/**
 * Method setupImgae
 */

- (void)setupImgae
{
    self.ivImage = [[UIImageView alloc] init];
    self.ivImage.frame = CGRectMake(0, KheightHeader, 320, 0);
    [self addSubview:self.ivImage];
    
    __weak typeof(self) weakSelf = self;
    
    [self.ivImage setImageWithURL:[NSURL URLWithString:[self.post urlImage]]
                 placeholderImage:nil
                          options:SDWebImageProgressiveDownload
                        completed: ^(UIImage *image, NSError *error,SDImageCacheType cacheType)
     {
        //resize and set image
        image = [image resizedImageByWidth:320];
        [weakSelf reloadedImage:image];
         
    }];
}

/**
 * Method getYTUrlStr
 */

- (NSString*)getYTUrlStr:(NSString*)url
{
    if (url == nil)
        return nil;
    
    NSString *retVal = [url stringByReplacingOccurrencesOfString:@"watch?v=" withString:@"v/"];
    
    NSRange pos=[retVal rangeOfString:@"version"];
    if(pos.location == NSNotFound)
    {
        retVal = [retVal stringByAppendingString:@"?version=3&hl=en_EN"];
    }
    return retVal;
}

/**
 * Method embedYouTube
 */

- (void)embedYouTube:(NSString *)urlString frame:(CGRect)frame {
    NSString *embedHTML = @"\
    <html><head>\
    <style type=\"text/css\">\
    body {\
    background-color: transparent;\
    color: white;\
    }\
    </style>\
    </head><body style=\"margin:0\">\
    <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
    width=\"%0.0f\" height=\"%0.0f\"></embed>\
    </body></html>";
    NSString *html = [NSString stringWithFormat:embedHTML,[self getYTUrlStr:urlString], frame.size.width, frame.size.height];
    
    self.wvVideo = [[UIWebView alloc] initWithFrame:frame];
    [self.wvVideo loadHTMLString:html baseURL:nil];
    self.wvVideo.scrollView.scrollEnabled = NO;
    self.wvVideo.scrollView.bounces = NO;
    
    [self addSubview:self.wvVideo];
    
    self.heightCell = KheightVideo + KheightHeader + kheightButtom;
    
    [self setFramex];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(reloadCell:index:height:)]) {
        [self.delegate reloadCell:self index:self.index height:self.heightCell];
    }
}

/**
 * Method reloadedImage
 */

- (void)reloadedImage:(UIImage*)image
{
    CGImageRef imgRef = [image CGImageWithCorrectOrientation];
    [self.ivImage setImage:image];
    
    self.heightCell = CGImageGetHeight(imgRef) + KheightHeader + kheightButtom;
    [self setFramex];
    
    self.ivImage.frame = CGRectMake(0, KheightHeader, 320, CGImageGetHeight(imgRef));
    
    // call reload tableview
    if (self.delegate && [self.delegate respondsToSelector:@selector(reloadCell:index:height:)]) {
        [self.delegate reloadCell:self index:self.index height:self.heightCell];
    }
}

- (void)setYButtomView
{
    NSLog(@"__%f__",self.heightCell - kheightButtom);
    [self.buttomview setFrame:CGRectMake(0, self.heightCell - kheightButtom, 320, kheightButtom)];
}

- (void)setFramex
{
    [self setFrame:CGRectMake(0, 0, 320, self.heightCell)];
    [self fetchDataButtomView];
    [self setYButtomView];
}

#pragma mark -
#pragma mark - tagert

/**
 * Method like
 */

- (void)like:(UIButton*)sender
{
    NSLog(@"like");
}

/**
 * Method comment
 */

- (void)comment:(UIButton*)sender
{
    NSLog(@"comment");
}

/**
 * Method save
 */

- (void)save:(UIButton*)sender
{
    NSLog(@"save");
    if ([self.post isImage]) {
          UIImageWriteToSavedPhotosAlbum(self.ivImage.image, nil, nil, nil);
    }
    
    else if ([self.post isVideo]) {
        [self download];
    }
  
}

/**
 * Method share
 */

- (void)share:(UIButton*)sender
{
    NSLog(@"share");
}

- (void)download {
//	[downloadButton setEnabled:NO];
	
//	[self.wvVideo setUserInteractionEnabled:NO];
    NSString *videoTitle = @"";
    
    UIUserInterfaceIdiom userInterfaceIdiom = [UIDevice currentDevice].userInterfaceIdiom;
    
    NSString *getURL = @"";
    
    if (userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        getURL = [self.wvVideo stringByEvaluatingJavaScriptFromString:@"function getURL() {var player = document.getElementById('player'); var video = player.getElementsByTagName('video')[0]; return video.getAttribute('src');} getURL();"];
    } else {
        getURL = [self.wvVideo stringByEvaluatingJavaScriptFromString:@"function getURL() {var bh = document.getElementsByClassName('bh'); if (bh.length) {return bh[0].getAttribute('src');} else {var zq = document.getElementsByClassName('zq')[0]; return zq.getAttribute('src');}} getURL();"];
    }
    
    NSString *getTitle = [self.wvVideo stringByEvaluatingJavaScriptFromString:@"function getTitle() {var jm = document.getElementsByClassName('jm'); if (jm.length) {return jm[0].innerHTML;} else {var lp = document.getElementsByClassName('lp')[0]; return lp.childNodes[0].innerHTML;}} getTitle();"];
    
	NSString *getTitleFromChannel = [self.wvVideo stringByEvaluatingJavaScriptFromString:@"function getTitleFromChannel() {var video_title = document.getElementById('video_title'); return video_title.childNodes[0].innerHTML;} getTitleFromChannel();"];
    
    //NSLog(@"%@, %@, %@", getURL, getTitle, getTitleFromChannel);
    
//	[self.wvVideo setUserInteractionEnabled:YES];
	
	NSArray *components = [getTitle componentsSeparatedByCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]];
	getTitle = [components componentsJoinedByString:@" "];
	
	if ([getURL length] > 0) {
		if ([getTitle length] > 0) {
			videoTitle = getTitle;
			
			bar = [[UIDownloadBar alloc] initWithURL:[NSURL URLWithString:getURL]
									progressBarFrame:CGRectMake(85.0, 17.0, 150.0, 11.0)
											 timeout:15
											delegate:self];
			
			[bar setProgressViewStyle:UIProgressViewStyleBar];
			
			[self addSubview:bar];
		} else {
			NSArray *components = [getTitleFromChannel componentsSeparatedByCharactersInSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]];
			getTitleFromChannel = [components componentsJoinedByString:@" "];
			
            if ([getTitleFromChannel length] == 0) {
                getTitleFromChannel = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSinceNow]];
            }
            
			if ([getTitleFromChannel length] > 0) {
				videoTitle = getTitleFromChannel;
				
				bar = [[UIDownloadBar alloc] initWithURL:[NSURL URLWithString:getURL]
										progressBarFrame:CGRectMake(85.0, 17.0, 150.0, 11.0)
												 timeout:15
												delegate:self];
				
				[bar setProgressViewStyle:UIProgressViewStyleBar];
				
				[self addSubview:bar];
			} else {
                //NSLog(@"%@", [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('html')[0].innerHTML;"]);
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"MyTube" message:@"Couldn't get video title." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alertView show];
                
//				[downloadButton setEnabled:YES];
			}
		}
	} else {
        //NSLog(@"%@", [webView stringByEvaluatingJavaScriptFromString:@"document.getElementsByTagName('html')[0].innerHTML;"]);
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"MyTube" message:@"Couldn't get MP4 URL." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
        
//		[downloadButton setEnabled:YES];
	}
}

#pragma mark -
#pragma mark - 

- (void)downloadBar:(UIDownloadBar *)downloadBar didFinishWithData:(NSData *)fileData suggestedFilename:(NSString *)filename {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *videoTitle = @"";
	[fileManager createFileAtPath:[NSString stringWithFormat:@"%@.mp4", [[paths objectAtIndex:0] stringByAppendingPathComponent:videoTitle]] contents:fileData attributes:nil];
	
	NSString *imagePath = [NSString stringWithFormat:@"%@.png", [[paths objectAtIndex:0] stringByAppendingPathComponent:videoTitle]];
	
	AVAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@.mp4", [[paths objectAtIndex:0] stringByAppendingPathComponent:videoTitle]]] options:nil];
	
	AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
	
	Float64 durationSeconds = CMTimeGetSeconds(asset.duration);
	
	CMTime midpoint = CMTimeMakeWithSeconds(durationSeconds / 2.0, 600);
	CMTime actualTime;
    
	CGImageRef preImage = [imageGenerator copyCGImageAtTime:midpoint actualTime:&actualTime error:NULL];
	
	if (preImage != NULL) {
		CGRect rect = CGRectMake(0.0, 0.0, CGImageGetWidth(preImage) * 0.5, CGImageGetHeight(preImage) * 0.5);
		
		UIImage *image = [UIImage imageWithCGImage:preImage];
		
		UIGraphicsBeginImageContext(rect.size);
		
		[image drawInRect:rect];
		
		NSData *data = UIImagePNGRepresentation(UIGraphicsGetImageFromCurrentImageContext());
		
		[fileManager createFileAtPath:imagePath contents:data attributes:nil];
		
		UIGraphicsEndImageContext();
	}
	
	CGImageRelease(preImage);

	
	[downloadBar removeFromSuperview];
    bar = nil;
}

- (void)downloadBar:(UIDownloadBar *)downloadBar didFailWithError:(NSError *)error {
	[downloadBar removeFromSuperview];
	
}

- (void)downloadBarUpdated:(UIDownloadBar *)downloadBar {}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
