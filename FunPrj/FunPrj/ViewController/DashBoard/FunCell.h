//
//  FunCell.h
//  FunPrj
//
//  Created by Thuy Dao on 2/9/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FunPost.h"
#import "UIView+Fun.h"
#import "UIDownloadBar.h"
#import <AVFoundation/AVFoundation.h>

@class FunCell;

@protocol cellDelegate<NSObject>
@optional

//setup funtion.
- (void)reloadCell:(FunCell*)funcell index:(NSInteger)index height:(CGFloat)height;

@end






@interface FunCell : UITableViewCell  <UIDownloadBarDelegate>

{
       UIDownloadBar *bar;
}

/**
 * property: buttomview
 */

@property (nonatomic, retain) UIView* buttomview;

/**
 * property: headerView
 */

@property (nonatomic, retain) UIView* headerView;

/**
 * property: funPost
 */

@property (nonatomic, retain) FunPost* post;

/**
 * property: heightCell
 */

@property (nonatomic, assign) CGFloat heightCell;

/**
 * property: ivImage
 */

@property (nonatomic, retain) UIImageView* ivImage;

/**
 * property: wvVideo
 */

@property (nonatomic, retain) UIWebView* wvVideo;

/**
 * property: delegate
 */

@property (nonatomic, retain) id <cellDelegate> delegate;

/**
 * property: index
 */

@property (nonatomic, assign) NSInteger index;

/**
 * Method setupImgae
 */

- (void)setupImgae;


/**
 * Method setup
 */

- (void)setup;

@end
