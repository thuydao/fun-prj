//
//  FunHorizontalViewController.m
//  FunPrj
//
//  Created by Thuy Dao on 2/9/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import "FunHorizontalViewController.h"

@interface FunHorizontalViewController ()

@end

@implementation FunHorizontalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - ProcessData

/**
 * Method refreshData
 */

- (void)refreshData
{
    
}

/**
 * Method seeMore
 */

- (void)seeMore
{
    
}



@end
