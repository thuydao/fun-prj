//
//  BaseSwipeViewController.h
//  SaleTracking
//
//  Created by Thuy Dao on 1/12/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FunDashBoardViewController.h"

@class BaseSwipeViewController;

@protocol SwipeDelegate <NSObject>
@optional

//setup funtion.
- (void)BaseSwipe:(BaseSwipeViewController*)swipe index:(NSInteger)index;

@end


@interface BaseSwipeViewController : FunDashBoardViewController


/**
 * property: delegate_
 */

@property (nonatomic, retain) id <SwipeDelegate> delegate_;

/**
 * property IBOutlet:myScrollView
 */

@property (nonatomic, retain) IBOutlet UIScrollView *myScrollView;

/**
 * property: all_ids
 */

@property (nonatomic, retain) NSMutableArray* all_ids;

/**
 * property: indexView
 */

@property (nonatomic, assign) NSInteger indexView;

/**
 * Method reloadTable
 */

- (void)reloadTable:(NSInteger)tag;

/**
 * Method getIndexViewFromTabelTag
 */

- (NSInteger)getIndexViewFromTabelTag:(NSInteger)tag;

/**
 * property: isSetuped
 */

@property (nonatomic, assign) BOOL isSetuped;

/**
 * Method addArr
 */

- (void)addArr:(NSArray*)arr;

@end
