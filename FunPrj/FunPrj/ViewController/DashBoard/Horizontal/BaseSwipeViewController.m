//
//  BaseSwipeViewController.m
//  SaleTracking
//
//  Created by Thuy Dao on 1/12/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import "BaseSwipeViewController.h"

@interface BaseSwipeViewController ()

@end

#define ktableTag 1000
#define xxxx(r,g,b)     ([UIColor colorWithRed:r/255. green:g/255. blue:b/255. alpha:1.0])
#define COLOR_ORGxxx (xxxx(254,153,49))

@implementation BaseSwipeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.indexView = 0;
    self.isSetuped = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    if (!self.isSetuped) {
//        [self setupScrollView];
//        [self pushData];
//    }
}

- (void)refreshData
{
    [super refreshData];
}

/*
 * Method setupScrollView
 */

- (void)setupScrollView
{
    CGFloat height = self.myScrollView.frame.size.height;
    CGFloat width = 320* self.all_ids.count;
    self.myScrollView.scrollEnabled = YES;
    self.myScrollView.pagingEnabled = YES;
    [self.myScrollView setContentSize:CGSizeMake( width,self.myScrollView.frame.size.height)];
    
    // setup many table in scrollview
    
    //init a parent view
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(self.indexView*320, 0, 320, height)];
    
    //init table view && setup
    UITableView* mytableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, height) style:UITableViewStylePlain];
    mytableView.dataSource = (id)self;
    mytableView.delegate = (id)self;
    mytableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mytableView.tag = ktableTag + self.indexView;
    
    // add table in to parent view
    [view setBackgroundColor:COLOR_ORGxxx];
    [view addSubview:mytableView];
    
    //add parent view in to scrollview
    [self.myScrollView addSubview:view];
    
    [self.myScrollView scrollRectToVisible:CGRectMake(320* self.indexView, self.myScrollView.frame.origin.y, self.myScrollView.frame.size.width, self.myScrollView.frame.size.height) animated:NO];
    
    [self checkForArrow];
    self.isSetuped = YES;
    
    [self runSetUpTable];
}

- (void)runSetUpTable
{
    UITableView *table1 = (UITableView*)[self.myScrollView viewWithTag:self.indexView + ktableTag + 1];
    if (table1 == nil) {
        [self addTable:self.indexView + 1];
    }
    UITableView *table2 = (UITableView*)[self.myScrollView viewWithTag:self.indexView + ktableTag - 1];
    if (table2 == nil) {
        [self addTable:self.indexView - 1];
    }
}

- (void)addTable:(NSInteger)index
{
    CGFloat height = self.myScrollView.frame.size.height;

    //init a parent view
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(index*320, 0, 320, height)];
    
    //init table view && setup
    UITableView* mytableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, height) style:UITableViewStylePlain];
    mytableView.dataSource = (id)self;
    mytableView.delegate = (id)self;
    mytableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mytableView.tag = ktableTag + index;
    
    // add table in to parent view
    [view setBackgroundColor:COLOR_ORGxxx];
    [view addSubview:mytableView];
    
    //add parent view in to scrollview
    [self.myScrollView addSubview:view];
}

/**
 * Method pushData
 */

- (void)pushData
{
    //check indexView safe
    if (![self isIndexViewSafe]) return;
    
    // get param
    NSString *idi = [NSString stringWithFormat:@"%@",[self.all_ids objectAtIndex:self.indexView]];
    NSMutableDictionary* info = [NSMutableDictionary new];
    [info setObject:idi forKey:@"id"];

    //show indicator
    
    // call api
    [self callAPIDetail];
}

/**
 * Method callAPIDetail
 */

- (void)callAPIDetail
{
}

/**
 * Method successData
 */

- (void)successData:(NSDictionary*)data
{
    
}

/**
 * Method scrollViewDidScroll
 */

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    int pageNum = (int)(sender.contentOffset.x / 320);
    if (pageNum != self.indexView) {
        self.self.indexView = pageNum;
        [self changePage];
        [self checkForArrow];
    }
}

- (void)checkForArrow
{
//    if(self.indexView == 0) [self setleft:NO];
//    else [self setleft:YES];
//    
//    
//    if(self.indexView == [self.all_ids count] - 1)
//    {
//        [self setright:NO];
//        if (self.delegate_ && [self.delegate_ respondsToSelector:@selector(BaseSwipe:index:)]) {
//            [self.delegate_ BaseSwipe:self index:self.indexView];
//        }
//    }
//    else [self setright:YES];
}

- (void)scrollToPage:(NSInteger)page
{
    [self.myScrollView scrollRectToVisible:CGRectMake(320 * page, self.myScrollView.frame.origin.y, self.myScrollView.frame.size.width, self.myScrollView.frame.size.height) animated:NO];
}

/**
 * Method changePage
 */

- (void)changePage
{
//    if (![self.dataSource objectForKey:[NSString stringWithFormat:@"%d",self.indexView]]) {
//        [self pushData];
//    }
//    else {
//        [self reloadTable:self.indexView];
//    }
//    
//    [self runSetUpTable];
}

/**
 * Method successData
 */

- (void)reloadTable:(NSInteger)tag
{
    dispatch_async( dispatch_get_main_queue(), ^{
        UITableView *table = (UITableView*)[self.myScrollView viewWithTag:tag + ktableTag];
        [table reloadData];
    });
    
}

/**
 * Method getIndexViewFromTabelTag
 */

- (NSInteger)getIndexViewFromTabelTag:(NSInteger)tag
{
    return tag - ktableTag;
}

/**
 * Method isIndexViewSafe
 */

- (BOOL)isIndexViewSafe
{
    if (self.indexView < 0 || self.indexView >= self.all_ids.count) return NO;
    return YES;
}

/**
 * Method left
 */

- (void)left:(UIButton*)sender
{
    self.indexView -= 1;
    if ([self isIndexViewSafe])
    {
        [self scrollToPage:self.indexView];
        [self changePage];
        [self checkForArrow];
    }
}

/**
 * Method right
 */

- (void)right:(UIButton*)sender
{
    self.indexView += 1;
    if ([self isIndexViewSafe])
    {
        [self scrollToPage:self.indexView];
        [self changePage];
        [self checkForArrow];
    }
}

#pragma mark -
#pragma mark - input

/**
 * Method addArr
 */

- (void)addArr:(NSArray*)arr
{
    [self.all_ids addObjectsFromArray:arr];

    //setup scrollview
    CGFloat width = 320* self.all_ids.count;
    [self.myScrollView setContentSize:CGSizeMake( width,self.myScrollView.frame.size.height)];
    
    //resetView
    [self checkForArrow];
    [self runSetUpTable];
}

@end
