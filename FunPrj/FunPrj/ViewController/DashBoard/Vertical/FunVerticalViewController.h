//
//  FunVerticalViewController.h
//  FunPrj
//
//  Created by Thuy Dao on 2/9/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FunDashBoardViewController.h"
#import "FunCell.h"

@interface FunVerticalViewController : FunDashBoardViewController <UITableViewDataSource, UITableViewDelegate,cellDelegate>

/**
 * property IBOutlet:myTable
 */

@property (nonatomic, retain) IBOutlet UITableView *myTable;

@end
