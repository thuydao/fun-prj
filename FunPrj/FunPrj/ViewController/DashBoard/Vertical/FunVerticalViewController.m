//
//  FunVerticalViewController.m
//  FunPrj
//
//  Created by Thuy Dao on 2/9/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import "FunVerticalViewController.h"


@interface FunVerticalViewController ()

@end

@implementation FunVerticalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupPull:self.myTable];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

#pragma mark -
#pragma mark - ProcessData

/**
 * Method refreshData
 */

- (void)refreshData
{
    [self.myTable reloadData];
}

/**
 * Method seeMore
 */

- (void)seeMore
{
    [self.myTable reloadData];
}

#pragma mark -
#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.dataSource count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        FunPost *postObj = [self.dataSource objectAtIndex:indexPath.section];
        NSString *cellIdentifier = [NSString stringWithFormat:@"vDashbroadCell%@",postObj.idi];
        FunCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[FunCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
            
            [self.myTable registerClass:[FunCell class] forCellReuseIdentifier:cellIdentifier];
            cell.post = postObj;
            cell.index = indexPath.section;
            cell.delegate = (id)self;
            [cell setup];
        }
        
        [cell disableSelected];
        
        return cell;
    }
    else if (indexPath.row == 1)
    {
        NSString *cellIdentifier = [NSString stringWithFormat:@"whileSpaceCell"];
        FunCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[FunCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
            [cell addSubview:view];
            [view setBackgroundColor:[UIColor grayColor]];
        }
        
        [cell disableSelected];
        
        return cell;

    }
    
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) return 30;
    FunPost *postObj = [self.dataSource objectAtIndex:indexPath.section];
    NSString *cellIdentifier = [NSString stringWithFormat:@"vDashbroadCell%@",postObj.idi];
    
    if ([self.dictCell objectForKey:cellIdentifier] == nil) {
        return 244;
    }
    else {
        CGFloat fl = [[self.dictCell objectForKey:cellIdentifier] floatValue];
        return fl;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"index:\n %ld", (long)indexPath.section);
}

#pragma mark -
#pragma mark - cellDelegate

- (void)reloadCell:(FunCell *)funcell index:(NSInteger)index height:(CGFloat)height
{
//    [self.myTable beginUpdates];
     NSString *cellIdentifier = [NSString stringWithFormat:@"vDashbroadCell%@",funcell.post.idi];
     [self.dictCell setObject:[NSString stringWithFormat:@"%f",height] forKey:cellIdentifier];
    
//    NSArray *reloadIndexPath = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]];
//    [self.myTable reloadRowsAtIndexPaths:reloadIndexPath withRowAnimation:UITableViewRowAnimationFade];
//    [self.myTable endUpdates];
//    NSRange range = NSMakeRange(index, 0);
//    NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
//    [self.myTable reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationRight];
    
    [self.myTable reloadData];
    
//
    NSLog(@"INDEX:\n %ld  %f", (long)index ,height);
}




@end
