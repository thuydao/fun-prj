//
//  UIView+Fun.h
//  FunPrj
//
//  Created by Thuy Dao on 2/11/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Fun)

/**
 * Method labelWithTag
 */

- (UILabel*)labelWithTag:(NSInteger)tag;

/**
 * Method imageViewWithTag
 */

- (UIImageView*)imageViewWithTag:(NSInteger)tag;

/**
 * Method buttonWithTag
 */

- (UIButton*)buttonWithTag:(NSInteger)tag;

/**
 * Method backgroundColorRed
 */

- (void)backgroundColorRed;

@end
