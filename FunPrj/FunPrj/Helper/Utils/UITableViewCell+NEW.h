//
//  UITableViewCell+VMODEV.h
//  TehuanPrj
//
//  Created by Thuy on 11/19/13.
//  Copyright (c) 2013 vmodev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UITableViewCell (NEW)

- (UIImageView*)ImageViewWithTag:(NSInteger)tag;
- (UILabel*)labelWithTag:(NSInteger)tag;
- (UIButton*)buttonWithTag:(NSInteger)tag;
- (UISwitch*)switchWithTag:(NSInteger)tag;

/*
 * Method disableSelected
 */

- (void)disableSelected;

/**
 * Method heightCell
 */

- (CGFloat)heightCell;

@end
