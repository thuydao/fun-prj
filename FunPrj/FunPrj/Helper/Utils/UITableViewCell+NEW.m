//
//  UITableViewCell+VMODEV.m
//  TehuanPrj
//
//  Created by Thuy on 11/19/13.
//  Copyright (c) 2013 vmodev. All rights reserved.
//

#import "UITableViewCell+NEW.h"

@implementation UITableViewCell (NEW)

- (UIImageView*)ImageViewWithTag:(NSInteger)tag
{
    return (UIImageView*)[self viewWithTag:tag];
}

- (UILabel*)labelWithTag:(NSInteger)tag
{
    return (UILabel*)[self viewWithTag:tag];
}

- (UIButton*)buttonWithTag:(NSInteger)tag
{
    return (UIButton*)[self viewWithTag:tag];
}

- (UISwitch*)switchWithTag:(NSInteger)tag
{
    return (UISwitch*)[self viewWithTag:tag];
}

/*
 * Method disableSelected
 */

- (void)disableSelected
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

/**
 * Method heightCell
 */

- (CGFloat)heightCell
{
    return self.frame.size.height;
}

@end
