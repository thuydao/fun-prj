//
//  UIView+Fun.m
//  FunPrj
//
//  Created by Thuy Dao on 2/11/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import "UIView+Fun.h"

@implementation UIView (Fun)

/**
 * Method labelWithTag
 */

- (UILabel*)labelWithTag:(NSInteger)tag
{
    return (id)[self viewWithTag:tag];
}

/**
 * Method buttonWithTag
 */

- (UIButton*)buttonWithTag:(NSInteger)tag
{
    return (id)[self viewWithTag:tag];
}

/**
 * Method imageViewWithTag
 */

- (UIImageView*)imageViewWithTag:(NSInteger)tag
{
    return (id)[self viewWithTag:tag];
}

/**
 * Method backgroundColorRed
 */

- (void)backgroundColorRed
{
    [self setBackgroundColor:[UIColor redColor]];
}

@end
