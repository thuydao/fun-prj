//
//  NSString+Fun.h
//  FunPrj
//
//  Created by Thuy Dao on 2/9/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Fun)

/**
 * Method string
 */

+ (NSString*)string:(id)value;

@end
