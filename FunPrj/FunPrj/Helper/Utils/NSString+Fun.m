//
//  NSString+Fun.m
//  FunPrj
//
//  Created by Thuy Dao on 2/9/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import "NSString+Fun.h"

@implementation NSString (Fun)

/**
 * Method string
 */

+ (NSString*)string:(id)value
{
    return (value != nil) ? [NSString stringWithFormat:@"%@",value] : @"";
}

@end
