//
//  FunDefine.h
//  FunPrj
//
//  Created by Thuy Dao on 1/15/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import <Foundation/Foundation.h>


#pragma mark -
#pragma mark - color

#define COLOR(r,g,b)     ([UIColor colorWithRed:r/255. green:g/255. blue:b/255. alpha:1.0])
#define COLOR_BACKGROUND_NAVIGATION     (COLOR(234,234,234))

#define KMainView @"MainView"
#define KMenuView @"MenuView"

#define KVDashBroad @"VDashBroad"












#pragma mark -
#pragma mark - setup

#define KHostImage @"http://s.hoibi.net/"
#define KBanerID @"a14ec317dca03ff"

#define KTest 1
