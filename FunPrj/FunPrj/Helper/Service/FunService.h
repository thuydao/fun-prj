//
//  Fun.h
//  FunPrj
//
//  Created by Thuy Dao on 2/10/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"

/**
 *  Defines
 */
#define ST_VERSION              @"1"
#define kUrlHost              @"http://hoibi.net/"
#define kDomainError          @"com.dp.error"
#define kCodeError            -1
#define kDescriptionError     @"DliveryPal exception error"
#define kKeyError             @"error"
#define KCookies              @"KCookies"


@interface NSString(json)

- (id)jsonObject:(NSError **)error;

@end

@implementation NSString(json)

- (id)jsonObject:(NSError **)error
{
    NSData *jsonData =  [self dataUsingEncoding:NSUTF8StringEncoding];
    id obj = [NSJSONSerialization JSONObjectWithData:jsonData
                                             options: NSJSONReadingMutableContainers
                                               error: error];
    
    return obj;
}

@end


@interface FunService : NSObject

+(id)shared;

/**
 *  Service's version
 */
+(NSString *) version;

/*
 * Method login
 */


#pragma mark -
#pragma mark - setup API

/*
 * Method service
 */

+ (void) service:(NSDictionary *)params
          prefix:(NSString*) prefix
       completed:(void(^)(id object,NSError *err)) completed;


@end
