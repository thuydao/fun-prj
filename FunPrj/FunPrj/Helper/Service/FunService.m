//
//  Fun.m
//  FunPrj
//
//  Created by Thuy Dao on 2/10/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import "FunService.h"

@implementation FunService

+(id)shared
{
    static FunService *sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:kUrlHost]];
    });
    return sharedInstance;
}

//- (void)saveCookies{
//
//    NSData *cookiesData = [NSKeyedArchiver archivedDataWithRootObject: [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject: cookiesData forKey: @"sessionCookies"];
//    [defaults synchronize];
//
//}
//
//- (void)loadCookies{
//
//    NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey: @"sessionCookies"]];
//    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//
//    for (NSHTTPCookie *cookie in cookies){
//        [cookieStorage setCookie: cookie];
//    }
//
//}

/**
 *  errorService
 */
+(NSError *) errorService:(NSString *)errorMessage
{
    NSError * error = [NSError errorWithDomain:kDomainError
                                          code:kCodeError
                                      userInfo:@{NSLocalizedDescriptionKey : errorMessage}];
    
    return error;
}

/**
 *  version
 */
+(NSString *) version
{
    return ST_VERSION;
}

/**
 *  handleResponseError
 */
+(NSError *) handleResponseDictionary:(NSDictionary *)responseDictionary inError:(NSError *)error
{
    //-- make sure nsdictionary class && not error
    if ([responseDictionary isKindOfClass:[NSDictionary class]] && error == nil) {
        
        //-- error from server
        if ([[responseDictionary objectForKey:@"errorCode"] integerValue] != 0) {
            error = [FunService errorService:[responseDictionary objectForKey:@"msg"]];
        }
    }
    else {
        //-- merge error
        error = error != nil ? error : [FunService errorService:kDescriptionError];
    }
    return error;
}


#pragma mark -
#pragma mark - setup API

+ (NSString*)paramsFromDictionary:(NSDictionary*)dict
{
    NSString *res = @"";
    NSMutableArray *arr = [NSMutableArray new];
    for (NSString *key in [dict allKeys]) {
        NSString *temp = [NSString stringWithFormat:@"%@=%@",key,[dict objectForKey:key]];
        [arr addObject:temp];
    }
    res = [arr componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"?%@",res];
}

/*
 * Method service
 */

+ (void) service:(NSDictionary *)params
          prefix:(NSString*) prefix
       completed:(void(^)(id object,NSError *err)) completed
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@""]];

    NSString *url = [NSString stringWithFormat:@"%@%@%@",kUrlHost,prefix,[self paramsFromDictionary:params]];
    NSLog(@"url :\n %@", url);
    
    [httpClient postPath:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //-- json string to nsdictionary
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        NSError *error = nil;
        NSDictionary *responseDictionary = [operation.responseString jsonObject:&error];
        
        //-- handle error
        error = [FunService handleResponseDictionary:responseDictionary inError:error];
        
        //-- callback
        if (completed != nil)
        {
            completed (responseDictionary, error);
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         completed (nil, error);
         NSLog(@"%@",[error description]);
     }];
    
}


@end
