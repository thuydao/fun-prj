//
//  main.m
//  FunPrj
//
//  Created by Thuy Dao on 1/15/14.
//  Copyright (c) 2014 Thuy Dao. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FunAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FunAppDelegate class]));
    }
}
